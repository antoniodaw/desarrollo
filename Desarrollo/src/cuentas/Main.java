package cuentas;

/**
 * Clase que crea una cuenta bancaria con unos datos determinados.
 * 
 * @author Antonio Cruz
 * @version 1.0.0
 *
 */

public class Main {

	public static void main(String[] args) {
		operativa_cuenta(0);
	}

	/**
	 * M�todo que crea una cuenta bancaria con unos datos determinados.
	 * 
	 * @param cantidad Importe con el que se va a crear la cuenta.
	 */
	private static void operativa_cuenta(float cantidad) {

		CCuenta cuenta1;
		double saldoActual;
		cuenta1 = new CCuenta("Antonio L�pez", "1000-2365-85-1230456789", 2500, 0);
		saldoActual = cuenta1.estado();
		System.out.println("El saldo actual es" + saldoActual);

		try {
			cuenta1.retirar(2300);
		} catch (Exception e) {
			System.out.print("Fallo al retirar");
		}
		try {
			System.out.println("Ingreso en cuenta");
			cuenta1.ingresar(695);
		} catch (Exception e) {
			System.out.print("Fallo al ingresar");
		}
	}

} // Fin de la clase