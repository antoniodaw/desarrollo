package cuentas;

/**
 * Clase que crea una nueva cuenta bancaria
 * 
 * @author Antonio Cruz
 * @version 1.0.0
 *
 */

public class CCuenta {

	private String nombre;
	private String cuenta;
	private double saldo;  
	private double tipoInteres;

	/**
	 * Constructor para que no falle al instanciar la clase
	 */
	public CCuenta() {
	}

	/**
	 * Constructor que se utiliza para crear una nueva cuenta con unos valores
	 * deteminados
	 * 
	 * @param nom  Nombre del cliente
	 * @param cue  Nombre de la cuenta
	 * @param sal  Saldo que hay en la cuenta
	 * @param tipo Tipo de cuenta.
	 */
	public CCuenta(String nom, String cue, double sal, double tipo) {
		nombre = nom;
		cuenta = cue;
		saldo = sal;
	}

	/**
	 * M�todo que se utiliza para ingresar una cantidad de dinero en una determinada
	 * cuenta bancaria.
	 * 
	 * @param cantidad Cantidad de dinero que se va a ingresar en la cuenta.
	 * @throws Exception que se lanza si se intenta ingresar una cantidad negativa.
	 */
	public void ingresar(double cantidad) throws Exception {
		if (cantidad < 0) {
		throw new Exception("No se puede ingresar una cantidad negativa");
		}
		saldo = saldo + cantidad;
	}

	/**
	 * M�todo que se utilizar para retirar una cantidad de dinero de una cuenta.
	 * 
	 * @param cantidad Cantidad de dinero que va a ser retirada de la cuenta.
	 * @throws Exception Se lanza si la cantidad a retirar es negativa o si se
	 *                   intenta retirar mas dinero del que hay en la cuenta.
	 */
	public void retirar(double cantidad) throws Exception {
		if (cantidad <= 0) {
			throw new Exception("No se puede retirar una cantidad negativa");
		}
		if (estado() < cantidad) {
			throw new Exception("No se hay suficiente saldo");
		}
		saldo = saldo - cantidad;
	}

	/**
	 * M�todo que devuelve en que estado se encuentra una cuenta, es decir el saldo
	 * que tiene disponible en el momento de la consulta.
	 * 
	 * @return saldo Importe que hay en la cuenta en el momento de la consulta.
	 */
	public double estado() {
		return saldo;
	}

	// Metodos Getters y Setters para encapsular los atributos de la clase.
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCuenta() {
		return cuenta;
	}

	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}

	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	public double getTipoInteres() {
		return tipoInteres;
	}

	public void setTipoInteres(double tipoInteres) {
		this.tipoInteres = tipoInteres;
	}

} // Fin de la clase
